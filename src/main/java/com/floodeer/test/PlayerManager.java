package com.floodeer.test;

import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private final Map<UUID, GamePlayer> onlinePlayers = Maps.newHashMap();

    public void addPlayer(UUID uuid) {
        if (!this.onlinePlayers.containsKey(uuid)) {
            final GamePlayer gamePlayer = new GamePlayer(uuid);
            onlinePlayers.put(uuid, gamePlayer);
        }
    }

    public void removePlayer(UUID uuid) {
        Main.get().getDataManager().savePlayer(onlinePlayers.get(uuid));
        onlinePlayers.remove(uuid);
    }

    /**
     * @return - Todos GamePlayers online
     */
    public Collection<GamePlayer> getAll() {
        return onlinePlayers.values();
    }

    /**
     * Limpar tudo, salvar os players sem executar novos Threads
     */
    public void shutdown() {
        getAll().forEach(gp -> Main.get().getDataManager().savePlayer(gp));
        onlinePlayers.clear();
    }

    /**
     * Salvar todos os players
     */
    public void updateAllAsync() {
        getAll().forEach(gp -> Main.get().getDataManager().savePlayerAsync(gp));
    }

    public GamePlayer getPlayer(UUID uuid) {
        return onlinePlayers.get(uuid);
    }

    public GamePlayer getPlayer(String name) {
        for (GamePlayer gPlayer: onlinePlayers.values()) {
            if (gPlayer.getPlayer().getName().equals(name)) {
                return gPlayer;
            }
        }
        return null;
    }
}
