package com.floodeer.test;

import org.bukkit.entity.Player;

import java.util.UUID;

public class GamePlayer {

    private UUID uuid;
    private int exp;
    private int level;
    private int money;

    public GamePlayer(UUID uuid) {
        this.uuid = uuid;

        Main.get().getDataManager().loadData(uuid.toString(), this); //Carregar valores
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Player getPlayer() {
        return Main.get().getServer().getPlayer(uuid);
    }
}
