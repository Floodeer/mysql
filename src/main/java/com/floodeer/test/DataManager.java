package com.floodeer.test;


import com.floodeer.test.database.SQLWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class DataManager {

    /**
     * Salvar os players usando um novo Thread fornecido pelo ExecutorService
     */
    public void savePlayerAsync(GamePlayer gp) {
        if(!Main.get().getDB().checkConnection())
            return;
        new SQLWriter() {
            Connection connection = Main.get().getDB().getConnection().get();
            PreparedStatement preparedStatement = null;

            @Override
            public void onWrite() throws SQLException {
                StringBuilder queryBuilder = new StringBuilder();

                //É bom atualizar o nome do player sempre já que ele pode mudar de nick
                queryBuilder.append("UPDATE `tabela_stats` SET ");
                queryBuilder.append("`playername` = ?, `money` = ?, `level` = ?, `exp` = ? ");
                queryBuilder.append("WHERE `uuid` = ?;");

                //Em ordem de tudo que foi selecionado
                preparedStatement.setString(1, gp.getPlayer().getName());
                preparedStatement.setInt(2, gp.getMoney());
                preparedStatement.setInt(3, gp.getLevel());
                preparedStatement.setInt(4, gp.getExp());
                preparedStatement.setString(5, gp.getUUID().toString());
                preparedStatement.executeUpdate();

                try {

                }finally {//Dados salvos, concluir operação
                    if(preparedStatement != null)
                        preparedStatement.close();
                }
            }
        }.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao salvar dados"); //Executar operação
    }

    /**
     * Mesmo método que savePlayerAsync, porém executado no Main Thread (causa lag)
     */
    public void savePlayer(GamePlayer gp) {
        if(!Main.get().getDB().checkConnection())
            return;
        Connection connection = Main.get().getDB().getConnection().get();
        PreparedStatement preparedStatement = null;

        StringBuilder queryBuilder = new StringBuilder();

        //É bom atualizar o nome do player sempre já que ele pode mudar de nick
        queryBuilder.append("UPDATE `tabela_stats` SET ");
        queryBuilder.append("`playername` = ?, `money` = ?, `level` = ?, `exp` = ? ");
        queryBuilder.append("WHERE `uuid` = ?;");

        //Em ordem de tudo que foi selecionado

        try {
            preparedStatement.setString(1, gp.getPlayer().getName());
            preparedStatement.setInt(2, gp.getMoney());
            preparedStatement.setInt(3, gp.getLevel());
            preparedStatement.setInt(4, gp.getExp());
            preparedStatement.setString(5, gp.getUUID().toString());
            preparedStatement.executeUpdate();
        }catch(SQLException ex) {

        }finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
            }catch (SQLException ex) {} //Ignore
        }
    }

    public void loadData(String uuid, GamePlayer player) {
        if(!Main.get().getDB().checkConnection())
            return;

        new SQLWriter() {
            Connection connection = Main.get().getDB().getConnection().get();
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;
            @Override
            public void onWrite() throws SQLException {
                if (!checkPlayer(uuid)) {
                    insertPlayer(uuid); //Inserir um novo player com valores padrões caso não exista
                }

                String query = "SELECT `money`, `level`, `exp` FROM `tabela_stats` WHERE `uuid` = " + uuid + " LIMIT 1;";
                preparedStatement = connection.prepareStatement(query);
                resultSet = preparedStatement.executeQuery();
                try {
                    if (resultSet != null && resultSet.next()) {
                        //Não há necessidade de checar se as colunas existem no resultSet; você selecionou elas manualmente
                        player.setMoney(resultSet.getInt("money"));
                        player.setExp(resultSet.getInt("Exp"));
                        player.setLevel(resultSet.getInt("level"));
                    }
                }finally { //Dados carregados, concluir operação
                    if(preparedStatement != null)
                        preparedStatement.close();
                    if(resultSet != null)
                        resultSet.close();
                }

            }
        }.writeOperation(Main.get().getDB().getExecutor(), Main.get().getLogger(), "Erro ao carregar dados."); //Executar operação
    }

    /**
     * Checar se o player na tabela
     */
    public boolean checkPlayer(String uuid) {
        if (!Main.get().getDB().checkConnection()) {
            return false;
        }

        int count = 0;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT Count(`player_id`) ");
            queryBuilder.append("FROM `tabela_stats` "); //Coloque o nome da tabela que estiver usando aqui
            queryBuilder.append("WHERE `uuid` = ? ");
            queryBuilder.append("LIMIT 1;");

            preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(queryBuilder.toString());
            preparedStatement.setString(1, uuid);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }

        } catch (final SQLException sqlException) {
            sqlException.printStackTrace();

        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (final SQLException ignored) {
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (final SQLException ignored) {
                }
            }
        }

        return count > 0;
    }


    /**
     * Inserir um novo player com valores padrões
     */
    public void insertPlayer(String uid) {
        if (!Main.get().getDB().checkConnection()) {
            return;
        }

        UUID uuid = UUID.fromString(uid);
        PreparedStatement preparedStatement = null;

        try {
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("INSERT INTO `tabela_stats` "); //Coloque o nome da tabela que estiver usando aqui
            queryBuilder.append("(`player_id`, `uuid`, `playername`) ");
            queryBuilder.append("VALUES ");
            queryBuilder.append("(NULL, ?, ?);");

            preparedStatement = Main.get().getDB().getConnection().get().prepareStatement(queryBuilder.toString());
            preparedStatement.setString(1, uid);
            preparedStatement.setString(2, Main.get().getServer().getPlayer(uuid).getName());
            preparedStatement.executeUpdate();

        } catch (final SQLException sqlException) {
            sqlException.printStackTrace();

        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (final SQLException ignored) {
                }
            }
        }
    }
}
