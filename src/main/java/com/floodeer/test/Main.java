package com.floodeer.test;

import com.floodeer.test.database.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.sql.SQLException;

public class Main  extends JavaPlugin implements Listener {

    private static Main main;
    private MySQL mysqlDatabase;
    private PlayerManager playerManager;
    private DataManager dataManager;

    public MySQL getDB() {
        return mysqlDatabase;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public static Main get() {
        return main;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        main = this;
        playerManager = new PlayerManager();
        dataManager = new DataManager();
        
        try {
            mysqlDatabase = new MySQL("localhost", "test", "root", "admin", 3306);
            mysqlDatabase.createTables(); //Iniciar mysql, tabelas
        }catch(IOException | SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        getServer().getPluginManager().registerEvents(this, this);

        getCommand("money").setExecutor(new CommandExecutor() { //Exemplo de uso do GamePlayer
            @Override
            public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
                if(!(command instanceof Player))
                    return true;

                Player sender = (Player)commandSender;
                GamePlayer gp = getPlayerManager().getPlayer(sender.getUniqueId());
                sender.sendMessage(ChatColor.YELLOW + "Saldo: " + gp.getMoney() + " coins.");

                return false;
            }
        });

        Bukkit.getScheduler().runTaskTimer(this, () -> { //Salvar dados a cada 5 minutos
            playerManager.updateAllAsync();
        }, 0, 20 * 300);
    }

    @Override
    public void onDisable() {
        super.onDisable();

        playerManager.shutdown();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Bukkit.getScheduler().runTaskLater(this, () -> playerManager.addPlayer(e.getPlayer().getUniqueId()), 20);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        playerManager.removePlayer(e.getPlayer().getUniqueId());
    }
}
